package com.example.victor.lesson_task_list.model;

import android.graphics.Color;

import com.example.victor.lesson_task_list.R;
import com.example.victor.lesson_task_list.utils.DataProvider;

/**
 * Created by victor on 20.03.17.
 */

public class CityViewModel {
    private String title;
    private WeatherState state;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public WeatherState getState() {
        return state;
    }

    public void setState(WeatherState state) {
        this.state = state;
    }

    public int getTemperature() {
        return state.getTemperature();
    }

    public int getWindSpeed() {
        return state.getWindSpeed();
    }

    public int getForecastIcon() {
        switch (state.getForecast()) {
            case DataProvider.STATE_CLEAR:
                return R.drawable.sun;
            case DataProvider.STATE_CLOUDY:
                return R.drawable.clouded;
            case DataProvider.STATE_RAINY:
                return R.drawable.storm;
            default:
                return R.drawable.snowing;
        }
    }

    public int getColorForecast(){
        switch (state.getForecast()){
            case DataProvider.STATE_CLEAR:
                return Color.argb(255,255,255,81);
            case DataProvider.STATE_CLOUDY:
                return Color.argb(255,17,233,233);
            case DataProvider.STATE_RAINY:
                return Color.argb(255,180,180,180);
            default:
                return Color.argb(255,239,227,227);
        }
    }

    public int getForecast() {
        switch (state.getForecast()) {
            case DataProvider.STATE_CLEAR:
                return R.string.forecast_sun;
            case DataProvider.STATE_CLOUDY:
                return R.string.forecast_cloudy;
            case DataProvider.STATE_RAINY:
                return R.string.forecast_rainy;
            default:
                return R.string.forecast_snow;
        }
    }
}
