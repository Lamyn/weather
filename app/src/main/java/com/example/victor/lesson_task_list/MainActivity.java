package com.example.victor.lesson_task_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.victor.lesson_task_list.adapters.CityAdapter;
import com.example.victor.lesson_task_list.model.CityViewModel;
import com.example.victor.lesson_task_list.model.WeatherState;
import com.example.victor.lesson_task_list.utils.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CityAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.cities);

        adapter = new CityAdapter(this);

        HashMap<String, WeatherState> data = DataProvider.getData();
        ArrayList<CityViewModel> cities = new ArrayList<>();
        for (String key : data.keySet()) {
            CityViewModel model = new CityViewModel();
            model.setTitle(key);
            model.setState(data.get(key));
            cities.add(model);
        }

        adapter.setData(cities);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
}
