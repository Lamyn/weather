package com.example.victor.lesson_task_list.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.victor.lesson_task_list.R;
import com.example.victor.lesson_task_list.model.CityViewModel;
import com.example.victor.lesson_task_list.model.WeatherState;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by victor on 20.03.17.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private ArrayList<CityViewModel> mDataList = new ArrayList<>();
    private Context mContext;

    public CityAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_item, parent, false);
        CityViewHolder holder = new CityViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        CityViewModel model = mDataList.get(position);
        holder.mTVTitle.setText(model.getTitle());
        holder.mTVTemperature.setText(mContext.getString(R.string.temperature, model.getTemperature()));
        holder.mTVWindSpeed.setText(mContext.getString(R.string.wind_speed, model.getWindSpeed()));
        holder.mIVForecast.setImageResource(model.getForecastIcon());
        holder.cardView.setCardBackgroundColor(model.getColorForecast());
        holder.mTVForecast.setText(mContext.getString(model.getForecast()));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void setData(ArrayList<CityViewModel> data) {
        mDataList = data;
    }

    class CityViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIVForecast;
        private CardView cardView;
        private TextView mTVTitle, mTVTemperature, mTVWindSpeed, mTVForecast;

        public CityViewHolder(View view) {
            super(view);
            mIVForecast = (ImageView) view.findViewById(R.id.iv_forecast);
            mTVForecast = (TextView) view.findViewById(R.id.tv_forecast);
            mTVTemperature = (TextView) view.findViewById(R.id.tv_temperature);
            mTVWindSpeed = (TextView) view.findViewById(R.id.tv_wind_speed);
            mTVTitle = (TextView) view.findViewById(R.id.tv_title);
            cardView =(CardView) view.findViewById(R.id.cardView);
        }
    }
}
